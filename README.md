# Proofpoint interview problem and solution

## Problem statement
Can you determine if a phone number is “easy to dial” based on each digit's physical location on a standard phone keypad?

Assume you have a standard phone keypad that looks like this:


```
1 2 3

4 5 6

7 8 9

  0
```

For the purposes of this problem, the digit “1” is considered adjacent to “1”, “2”, “5” and “4”. 
The digit “5” is considered adjacent to every digit except “0”. We consider a phone number “easy to dial” if every digit 
is physically adjacent to the next digit on the keypad. The phone numbers “254-7096” and “554-7521” are easy to dial. 
The phone numbers “280-6547” and “355-8123” are not.

## Approach
The phone dial pad can be thought of as a graph and each number then becomes a vertex. Each number which has 1 degree separation
can be represented with an adjacency list. Each of the numbers are stored in a `HashMap` which makes look ups constant time and 
if the adjacency list is initialized in sorted fashion, it is possible to perform binary search on the list to find out if the
number next to the current one is 1 degree away from the current number. This operation is `O(log N)` with N being number of adjacent entries. 
The HashMap of the numbers is initialized as a singleton so that multiple instances of `PhoneNumberChecker` class use same `NumberPad`. 
The `PhonerNumberChecker#isEasyToDial` function checks if the passed string is an easy to dial number or not. The function has 
guard against null string, invalid characters and it accepts '-' as a separator between segments of the phone number. 
There are some unit tests provided for all functions in `PhoneNumberChecker`. The overall time complexity of checking if a number is
`O(m log n)` where m is the number of digits and n is the adjacency list for any specific number.

## To run and test
To run the `PhoneNumberChecker` class which is main driver for the code, run
```
sbt run
```

To run with test cases, run
```
sbt test
```

To create a fat jar with all dependencies
```
sbt assembly
```