name := "proofpoint-test"

version := "0.1"

scalaVersion := "2.11.12"

libraryDependencies ++= Seq (
  "org.slf4j" % "slf4j-api" % "1.7.21",
  "org.slf4j" % "slf4j-simple" % "1.7.21",
  "junit" % "junit" % "4.10",
  "org.scalactic" %% "scalactic" % "3.0.5",
  "org.scalatest" %% "scalatest" % "3.0.5" % "test",
  "com.novocode" % "junit-interface" % "0.11" % "test"
)

val mainClassRun = Some("com.proofpoint.PhoneNumberChecker")
val mainClassTest = Some("com.proofpoint.PhoneNumberCheckerTest")

mainClass in (Compile, run) := mainClassRun
mainClass in assembly := mainClassRun
mainClass in Test := mainClassTest