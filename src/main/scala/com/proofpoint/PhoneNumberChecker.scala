package com.proofpoint

import org.slf4j.{Logger, LoggerFactory}

import scala.collection.Searching._

/**
  * A singleton which contains sorted proximity vectors
  */
private object NumberPad {
  val keyMap: Map[Int, Vector[Int]] = Map(
    0 -> Vector(7, 8, 9),
    1 -> Vector(2, 4, 5),
    2 -> Vector(1, 3, 4, 5, 6),
    3 -> Vector(2, 5, 6),
    4 -> Vector(1, 2, 5, 7, 8),
    5 -> Vector(1, 2, 3, 4, 6, 7, 8, 9),
    6 -> Vector(2, 3, 5, 8, 9),
    7 -> Vector(0, 4, 5, 8),
    8 -> Vector(0, 4, 5, 6, 7, 9),
    9 -> Vector(0, 5, 6, 8)
  )
}

class PhoneNumberChecker {
  /**
    * Checks if a two numbers are reachable. A number is always reachable from itself
    *
    * @param input1 First number
    * @param input2 Second number
    * @return If the numbers are close to each other on the keypad
    */
  private def isReachable(input1: Char, input2: Char): Boolean = {
    if (input1.isDigit && (input2 == '-' || input1 == input2) || input1 == '-' && input2.isDigit) //a digit from - at distance 1 is reachable
      true
    else {
      //Locate the proximity list and perform binary search on it. handles invalid characters with a false
      NumberPad.keyMap.get(input1.asDigit).exists(_.search(input2.asDigit).isInstanceOf[Found])
    }
  }

  /**
    * Checks if a provided phone number is easy to dial. A number can contain hyphen as a separator
    *
    * @param phoneNumber String representation of the number
    * @return if the number is easy to dial or not
    */
  def isEasyToDial(phoneNumber: String): Boolean = {
    if (phoneNumber == null || phoneNumber.isEmpty || phoneNumber.trim == "-")
      false
    else {
      val trimmedNum = phoneNumber.trim
      var hasInvalidCharacter = false
      for (i <- 0 until trimmedNum.length - 1 if !hasInvalidCharacter)
        hasInvalidCharacter = !isReachable(trimmedNum(i), trimmedNum(i + 1))
      !hasInvalidCharacter
    }
  }
}

/**
  * Driver class which contains main
  */
object PhoneNumberChecker extends App {
  private val logger: Logger = LoggerFactory.getLogger(this.getClass.getName)
  val phoneNumberChecker: PhoneNumberChecker = new PhoneNumberChecker
  val inputPhoneNumbers: Vector[String] = Vector("254-7096", "554-7521", "280-6547", "335-8123")

  inputPhoneNumbers.foreach { number =>
    logger.info(s"Checking if $number is easy to dial: ${phoneNumberChecker.isEasyToDial(number)}")
  }
}