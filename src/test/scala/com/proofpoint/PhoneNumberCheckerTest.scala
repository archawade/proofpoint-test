package com.proofpoint

import org.scalatest._
import org.scalatest.MustMatchers._
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class PhoneNumberCheckerTest extends FunSuite with PrivateMethodTester {
  private val numberChecker = new PhoneNumberChecker
  private val isReachable = PrivateMethod[Boolean]('isReachable)
  test("PhoneNumberChecker.isEasyToDial should be able to handle valid input") {
    val sampleInput = Vector("254-7096", "554-7521", "280-6547", "335-8123")
    val expectedOutput = Vector(true, true, false, false)
    val result = sampleInput.map(numberChecker.isEasyToDial)
    result mustEqual expectedOutput
  }

  test("PhoneNumberChecker.isEasyToDial should be able to handle invalid input") {
    val sampleInput = Vector("254---96", "----7521", "", null)
    val expectedOutput = Vector(false, false, false, false)
    val result = sampleInput.map(numberChecker.isEasyToDial)
    result mustEqual expectedOutput
  }

  test("PhoneNumberChecker.isReachable should be able to handle detect number proximity correctly") {
    val sampleInput = Vector(('1','1'),('2','5'),('0','2'),('5','6'))
    val expectedOutput = Vector(true, true, false, true)
    val result = sampleInput.map(entry => numberChecker invokePrivate isReachable(entry._1, entry._2))
    result mustEqual expectedOutput
  }
}
